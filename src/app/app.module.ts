import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ContextMenuService, SortService, TreeGridModule, FilterService, FreezeService, ResizeService, EditService, PageService, RowDDService } from '@syncfusion/ej2-angular-treegrid';
import { DropDownListAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { DialogModule } from '@syncfusion/ej2-angular-popups';


import { AppComponent } from './app.component';
import { TreeGridComponent } from './components/tree-grid/tree-grid.component';
import { AddEditModalComponent } from './components/add-edit-modal/add-edit-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePickerAllModule } from '@syncfusion/ej2-angular-calendars';
import { ColorPickerModule, NumericTextBoxAllModule } from '@syncfusion/ej2-angular-inputs';
import { HttpClientModule } from '@angular/common/http';
import { InputComponent } from './components/input/input.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';

const config: SocketIoConfig = { url: `${environment.serverAddress}:${environment.socketPort}`, options: {
  transports: ["websocket"]
} };

@NgModule({
  declarations: [
    AppComponent,
    TreeGridComponent,
    AddEditModalComponent,
    InputComponent,
  ],
  imports: [
    BrowserModule,
    TreeGridModule,
    DropDownListAllModule,
    DialogModule,
    ReactiveFormsModule,
    FormsModule,
    NumericTextBoxAllModule,
    DatePickerAllModule,
    ColorPickerModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    ButtonModule
  ],
  providers: [SortService, ContextMenuService, FilterService, FreezeService, ResizeService, EditService, PageService, RowDDService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
