import { Component, OnDestroy } from '@angular/core';
import { ColumnsService } from './services/columns.service';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  public get loaded(): boolean {
    return this._columnsService.loaded && this._dataService.loaded;
  }

  constructor(
    private _columnsService: ColumnsService,
    private _dataService: DataService,
  ) {}

  public ngOnDestroy(): void {
    this._dataService.dataServiceColumnSubscription?.unsubscribe();
  }
}
