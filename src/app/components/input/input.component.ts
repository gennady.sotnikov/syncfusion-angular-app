import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataTypes, IInputType } from '../../models/column';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  @Input() public dataType: IInputType = DataTypes.Text;

  @Input() public control: FormControl | null = null;

  @Input() public name: string = '';

  @Input() public label: string = '';

  @Input() public dataSource: string[] = [];

  public get inputErrorText() {
    const control = this.control;
    const keys = Object.keys(control?.errors || {}).filter((key) => !!(control?.errors && control?.errors[key]));
    const errorValue = control?.errors && control.errors[keys[0]];
    return errorValue?.value || 'This field is required!';
  }
}
