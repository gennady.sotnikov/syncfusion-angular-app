import { Component, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AddEventArgs, ColumnDragEventArgs, ContextMenuItemModel, DeleteEventArgs, EditEventArgs, FilterEventArgs, ResizeArgs, RowDragEventArgs, RowSelectEventArgs, SaveEventArgs, SearchEventArgs, SortEventArgs, SortSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ColumnChooserService, ReorderService, RowDDService, ToolbarService, TreeGridComponent as EJ2TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { MenuEventArgs } from '@syncfusion/ej2-navigations';
import { Column } from '@syncfusion/ej2-treegrid';
import { Subject } from 'rxjs';
import { pairwise, takeUntil } from 'rxjs/operators'
import { AddEditModalComponent } from '../add-edit-modal/add-edit-modal.component';
import { ColumnsService, Fields } from '../../services/columns.service';
import { DataService } from '../../services/data.service';
import { SocketService } from '../../services/socket.service';
import { IData, IDataInDataSource } from '../../models/data';
import { copiedRowsColor, extraGridHeight, rowHeight } from '../../constants/utils';
import { IColumn } from '../../models/column';

interface CorrectColumn extends Column {
  index: number;
}

interface CorrectMenuEventArgs extends MenuEventArgs {
  column?: CorrectColumn;
  rowInfo?: IData;
  items: unknown[];
}

type DynamicCMenuItem = {
  id: string;
  defaultText: string;
  getState: () => boolean;
};

enum HeaderMenuIds {
  Edit = 'edit',
  Filter = 'filter',
  Freeze = 'freeze',
  MSort = 'mSort',
  Show = 'show',
  DeleteColumn = 'deleteColumn',
  New = 'new',
}

enum ItemMenuIds {
  MultiSelection = 'mSelection',
  Cut = 'cut',
  Copy = 'copy',
  PasteSubling = 'pasteSub',
  PasteChild = 'pasteCh',
  DeleteRecord = 'deleteRecord'
}

type OnActionBegin = FilterEventArgs | SortEventArgs | SearchEventArgs | AddEventArgs | SaveEventArgs | EditEventArgs | DeleteEventArgs;

@Component({
  selector: 'app-tree-grid',
  templateUrl: './tree-grid.component.html',
  styleUrls: ['./tree-grid.component.scss'],
  providers: [ReorderService, ColumnChooserService, ToolbarService, RowDDService],
  encapsulation: ViewEncapsulation.None
})
export class TreeGridComponent implements OnInit, OnDestroy {

  public allowFiltering: boolean = true;

  public height = this._getHeight();

  public modalIsHidden = true;

  public columns: IColumn[] = [];

  public allFields: string[] = [];

  public data = this._dataService.data;

  public mSort: boolean = true;

  public frozenColumns: number = 0;

  public contextMenuItems: Array<string | ContextMenuItemModel> = [];

  public mSelect: boolean = false;

  public sortSettings: SortSettingsModel = { columns: [{ field: 'taskID', direction: 'Ascending' }] };

  public fields = Fields;

  public addEditForm: FormGroup | null = null;

  public selectedRows: IDataInDataSource[] = [];

  @ViewChild('modal') private _modal: AddEditModalComponent | null = null;
  @ViewChild('treeGrid') private _treeGridRef: EJ2TreeGridComponent | null = null;

  private _destroy$ = new Subject();

  private _columnMenuIndex: number = -1;

  private _isFalseSelection = false;

  private _dynamicCMenuItems: DynamicCMenuItem[] = [{
    defaultText: 'Multi-Sort',
    id: HeaderMenuIds.MSort,
    getState: () => this.mSort
   }, {
    defaultText: 'Filter',
    id: HeaderMenuIds.Filter,
    getState: () => this.allowFiltering
   }, {
     defaultText: 'Freeze',
     id: HeaderMenuIds.Freeze,
     getState: () => this.frozenColumns > this._columnMenuIndex
   }, {
     defaultText: 'Multi Select',
     id: ItemMenuIds.MultiSelection,
     getState: () => this.mSelect
   }];

  private _moveFromIndexes: Array<number[]> = [];

  private _movedRows: IDataInDataSource[] = [];

  private _isMoving = false;

  private _updateCount = 0;

  constructor(
    private _dataService: DataService,
    private _columnsService: ColumnsService,
    private _socketService: SocketService,
    private _fb: FormBuilder,
  ) { }

  @HostListener('window:resize')
  public onWindowResize() {
    this.height = this._getHeight();
  }

  @HostListener('contextmenu', ['$event'])
  public onClick(e: MouseEvent) {
    const target = e.target as HTMLElement;
    if (target && ['e-rowcelldrag', 'e-rowdragdrop'].some(className => target.classList.contains(className)) ) {
      const tr = target.nodeName === 'TD' ? target.parentElement : target.parentElement?.parentElement;
      const firstTdWithData = tr?.children.item(1) as HTMLElement;
      if (firstTdWithData) {
        firstTdWithData.click();
      }
    }
  }

  public getAddEditControl(key: string): FormControl {
    return this.addEditForm?.get(key) as FormControl;
  }

  public openModal(c?: IColumn) {
    this.modalIsHidden = false;
    setTimeout(() => {
      this._modal?.openDialog(c);
    }, 1000);
  }

  public contextMenuClick(args?: CorrectMenuEventArgs): void {
    switch(args?.item.id) {
      case HeaderMenuIds.Edit: {
        if (this._columnMenuIndex !== -1) {
          const editedColumn = this._columnsService.getColumn(this.columns[this._columnMenuIndex].name);
          if (!editedColumn) throw new Error('Edited column doesn\'t exists!');
          this.openModal(editedColumn);
        }
        break;
      }
      case HeaderMenuIds.New: {
        this.openModal();
        break;
      }
      case HeaderMenuIds.DeleteColumn: {
        if (this._columnMenuIndex !== -1) {
          this._columnsService.removeColumn(this.columns[this._columnMenuIndex]?.name);
        }
        break;
      }
      case HeaderMenuIds.MSort: {
        this.mSort = !this.mSort;
        break;
      }
      case HeaderMenuIds.Filter: {
        this.allowFiltering = !this.allowFiltering;
        this.height = this._getHeight();
        break;
      }
      case HeaderMenuIds.Freeze: {
        if (this._columnMenuIndex !== -1)
          this.frozenColumns = (this.frozenColumns - 1) >= this._columnMenuIndex
            ? 0
            : this._columnMenuIndex + (this._columnMenuIndex === 5 ? 0 : 1);
        break;
      }
      case ItemMenuIds.MultiSelection: {
        this.mSelect = !this.mSelect;
        break;
      }
      case ItemMenuIds.Cut: {
        this._dataService.copy(this.selectedRows, true);
        break;
      }
      case ItemMenuIds.Copy: {
        this._dataService.copy(this.selectedRows, false);
        break;
      }
      case ItemMenuIds.PasteSubling: {
        const targetRowIndex = this._dataService.countRowIndex(this.selectedRows[0]);
        this._dataService.paste(targetRowIndex, false);
        this._treeGridRef?.refresh();
        break;
      }
      case ItemMenuIds.PasteChild: {
        const targetRowIndex = this._dataService.countRowIndex(this.selectedRows[0]);
        this._dataService.paste(targetRowIndex, true);
        this._treeGridRef?.refresh();
        break;
      }
      case ItemMenuIds.DeleteRecord: {
        this._dataService.deleteRow(this.selectedRows.map(this._dataService.countRowIndex));
        break;
      }
      default: {
        break;
      }
    }
  }

  public ngOnInit(): void {
    this._socketService.getMessage$().pipe(takeUntil(this._destroy$)).subscribe(() => {
      if (!this._updateCount) {
        this._updateCount++;
        alert('Somebody just updated the data. Please, press \'Ok\' to get the updates.');
        document.body.click();
        this._columnsService.getColumns();
        this._dataService.getData();
        this._updateCount--;
      }
    });
    this._columnsService.columns$.pipe(takeUntil(this._destroy$)).subscribe(columns => {
      this.columns = columns;
      this.allFields = columns.map(field => field.name);
      this.contextMenuItems = this._getContextMenuItems();
    });
    this._columnsService.columns$.pipe(takeUntil(this._destroy$), pairwise()).subscribe(() => {
      // TODO: improve solution
      setTimeout(() => {
        this._treeGridRef?.refresh();
      }, 100);
    });
  }

  public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public onRowDragStart(e: RowDragEventArgs) {
    if (e?.data) {
      this._movedRows = this._dataService.filterChildren(e.data as IDataInDataSource[]);
      this._moveFromIndexes = this._dataService.filterChildren(this._movedRows).map(this._dataService.countRowIndex);
    }
  }

  public onRowDrop() {
    this._isMoving = true;
  }

  public onSelectionChange(e: RowSelectEventArgs) {
    if (!this._isFalseSelection) this.selectedRows = Array.isArray(e.data) ? e.data as IDataInDataSource[] : [e.data as IDataInDataSource];
    this._isFalseSelection = false;
  }

  public onResize(e: ResizeArgs) {
    if (e.column) {
      const { field, width } = e.column;
      this._columnsService.editWidth(field, +(width as string).replace('px', ''));
    }
  }

  public onActionBegin(e: OnActionBegin) {
    switch(e.requestType) {
      case 'add': {
        e = e as AddEventArgs;
        this._buildAddEditForm(e.rowData as IData | undefined, true);
        this._isFalseSelection = true;
        break;
      }
      case 'beginEdit': {
        e = e as AddEventArgs;
        this._buildAddEditForm(e.rowData as IData | undefined, false);
        break;
      }
      case 'save': {
        this._handleStartSave(e as SaveEventArgs);
        break;
      }
      case 'refresh': {
        if (this._isMoving) {
          this._handleRowMoved();
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  public onActionEnd(e: SaveEventArgs) {
    if (e.requestType === 'save') this._handleEndSave(e);
  }

  public onReorder(e: ColumnDragEventArgs) {
    if (e.column && e.target) {
      const thEl = this._getThEl(e.target);
      if (thEl) {
        const index = Array.from(thEl?.parentNode?.children || []).indexOf(thEl) - 1; // there is an extra column for moving rows
        this._columnsService.reorderColumn(e.column.field, index);
      }
    }
  }

  public onHeaderMenuOpen(event: CorrectMenuEventArgs) {
    const selectedCell = event.rowInfo?.cell as HTMLElement;
    if (event.rowInfo?.rowData && !(!selectedCell || selectedCell?.classList.contains('e-rowdragdropcell'))) {
      (event as any).cancel = true;
    }

    if (event.column) this._columnMenuIndex = event.column?.index;
    this._dynamicCMenuItems
      .map((item) => document.getElementById(item.id))
      .forEach((item, index) => {
        const { defaultText, getState } = this._dynamicCMenuItems[index];
        if (item?.innerText) {
          item.innerText = `${defaultText} ${getState() ? 'on' : 'off'}`
        }
    });
  }

  public getBackgroundColor(data: IData, column: IColumn) {
    return this._dataService.rowsInBuffer.some(rInBuffer => data.uniqueID === rInBuffer.uniqueID) ? copiedRowsColor : column.backgroundColor;
  }

  private _handleRowMoved() {
    this._isMoving = false;
    const movedFrom = this._moveFromIndexes;
    const movedTo = this._dataService.filterChildren(this._movedRows).map(this._dataService.countRowIndex); // wrong
    this._dataService.moveRows(movedFrom, movedTo);
  }

  private _handleStartSave(e: SaveEventArgs) {

    const formValues = this.addEditForm?.value;
    e.data = { ...e.data, ...formValues as Object };
    if (e.action === 'edit') {
      const index = this._dataService.countRowIndex(this.selectedRows[0] as IDataInDataSource);
      this._dataService.editRow(index, formValues);
    }
  }

  private _handleEndSave(e: SaveEventArgs) {
    if (e.action === 'add' && this.selectedRows.length) {
      const target = this.selectedRows[0];
      const eventData = e.data as IDataInDataSource;
      const newEl = eventData.taskData;
      const isChild = eventData?.parentItem?.uniqueID === target?.uniqueID;
      if (isChild) {
        this._dataService.addAsChild(this._dataService.countRowIndex(target), newEl);
      } else {
        const parent = target.level ? target.parentItem?.subtasks : this._dataService.data.dataSource.json;
        const targetComplexIndex = this._dataService.countRowIndex(target);
        const targetIndex = parent?.indexOf(target.taskData);
        const newIndex = parent?.indexOf(newEl);
        if (newIndex !== undefined && targetIndex !== undefined && targetIndex !== -1 && newIndex !== -1) {
          const isNext = newIndex > targetIndex;
          if (!isNext) {
            targetComplexIndex[targetComplexIndex.length - 1] = targetComplexIndex[targetComplexIndex.length - 1] - 1;
          }
          this._dataService.addAsSubling(targetComplexIndex, newEl, isNext);
        }
      }
    }
  }

  private _buildAddEditForm(eventRowData: IData | undefined, isAdd: boolean) {

    const data = isAdd ? this._columnsService.defaultData : eventRowData;
    if (data) {
      this.addEditForm = this._fb.group(this.columns.reduce((accum, column) => {
        const newValue = data[column.name] === undefined ? column.defaultValue : data[column.name];
        accum[column.name] = [newValue, Validators.required];
        return accum;
      }, {} as any));
    }
  }

  private _getThEl(el: Element | null): Element | null | undefined {
    switch (el?.nodeName) {
      case 'TH': return el;
      case 'DIV': return el.classList.contains('app-header-cell') ? el.parentElement?.parentElement : el.parentElement;
      case 'SPAN': return  el.parentElement?.parentElement;
      default: return null;
    }
  }

  private _getContextMenuItems() {
    return [
      'AddRow',
      'Edit',
      { text: 'Delete record(s)', target: '.e-content', id: ItemMenuIds.DeleteRecord },
      { text: 'Copy', target: '.e-content', id: ItemMenuIds.Copy },
      { text: 'Cut', target: '.e-content', id: ItemMenuIds.Cut },
      { text: 'Paste Next', target: '.e-content', id: ItemMenuIds.PasteSubling },
      { text: 'Paste Child', target: '.e-content', id: ItemMenuIds.PasteChild },
      { text: 'Multi-Select', target: '.e-content', id: ItemMenuIds.MultiSelection },
      { text: 'New Column', target: '.e-columnheader', id: HeaderMenuIds.New },
      { text: 'Edit Column', target: '.e-columnheader', id: HeaderMenuIds.Edit },
      { text: 'Delete Column', target: '.e-columnheader .e-headercelldiv :not(.pk)', id: HeaderMenuIds.DeleteColumn },
      { text: 'Multi-Sort', target: '.e-columnheader', id: HeaderMenuIds.MSort },
      { text: 'Filter', target: '.e-columnheader', id: HeaderMenuIds.Filter },
      { text: 'Freeze', target: '.e-columnheader', id: HeaderMenuIds.Freeze }
    ];
  }

  private _getHeight(): number {
    return window.innerHeight - (this.allowFiltering ? 3 : 2) * rowHeight - extraGridHeight;
  }
}
