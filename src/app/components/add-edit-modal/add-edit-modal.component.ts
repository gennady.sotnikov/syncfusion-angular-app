import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataTypes, IColumn, IColumnInput, IGeneralSettingsInput, IInputType, InputTypes, TextAlign, TextWrap } from '../../models/column';
import { ColumnsService } from '../../services/columns.service';
import { defaultBackgroundColor, defaultFontColor, defaultFontSize, defaultMinWidth, defaultText } from '../../constants/utils';

@Component({
  selector: 'app-add-edit-modal',
  templateUrl: './add-edit-modal.component.html',
  styleUrls: ['./add-edit-modal.component.scss']
})
export class AddEditModalComponent implements OnInit, OnDestroy {

  @ViewChild('ejDialog') public ejDialog: DialogComponent | null = null;

  public targetElement = document.body;

  public validationErrors: { [key: string]: string[] } = { columnName: [] };

  public column$ = new BehaviorSubject<IColumn | null>(null);

  public dataTypes = DataTypes;

  public colorType: IInputType = InputTypes.Color;

  public dataTypesDataSource = [DataTypes.Text, DataTypes.Num, DataTypes.DropDownList, DataTypes.Date, DataTypes.Boolean];

  public isEdit: boolean = false;

  public textAligns = TextAlign;

  public textWrapDataSource = [TextWrap.Normal, TextWrap.BreakWord];

  public textAlignDataSource = [TextAlign.Center, TextAlign.Left, TextAlign.Right];

  public generalValInput: IGeneralSettingsInput = { dataType: DataTypes.Text, defaultValue: defaultText };


  private readonly _booleanDataSource = Object.freeze(['true', 'false']);

  private readonly _dropdownListDataSource = ['Option 1', 'Option 2', 'Option 3'];

  private readonly _initialValue: IColumnInput = Object.freeze({
    headerText: 'New Column',
    minWidth: defaultMinWidth,
    textAlign: TextAlign.Center,
    dataType: DataTypes.Text,
    defaultValue: defaultText,
    textWrap: TextWrap.Normal,
    fontSize: defaultFontSize,
    fontColor: defaultFontColor,
    backgroundColor: defaultBackgroundColor,
    dataSource: []
  });

  private get _initialEditForm() {
    return this._fb.group(
      Object.keys(this._initialValue).reduce((accum, key) => {
        const value = this._initialValue[key as keyof IColumnInput];
        if (typeof value === 'object' && Array.isArray(value)) {
          accum[key] = this._fb.array(value.map(value => this._fb.control(value, Validators.required)))
        } else {
          accum[key] = [value, Validators.required];
        }
        return accum;
      }, {} as any)
    );
  }

  private get _optionsValues() {
    return this.dataSourceControl.controls.map(control => control.value);
  }

  public editForm: FormGroup = this._initialEditForm;

  private get _field(): string {
    return this.column$.value?.name || '';
  }

  private get _columnHeaderText(): string {
    return this.column$.value?.headerText || '';
  }

  public get dataSourceControl() {
    return this.editForm.controls.dataSource as FormArray;
  }

  private readonly _destroy$ = new Subject();

  constructor(
    private _columnsService: ColumnsService,
    private _fb: FormBuilder,
  ) {
    this.editForm.controls.headerText.setValidators([this.columnNameValidating]);
      this.editForm.controls.dataType.valueChanges.pipe(takeUntil(this._destroy$)).subscribe(val => {
      switch (val) {
        case DataTypes.Date: {
          this.editForm.controls.defaultValue.setValue(new Date());
          break;
        }
        case DataTypes.Num: {
          this.editForm.controls.defaultValue.setValue(0);
          break;
        }
        case DataTypes.Boolean: {
          this.editForm.controls.dataSource = this._fb.array(this._booleanDataSource.map(e => this._fb.control([e])));
          this.editForm.controls.defaultValue.setValue('false');
          break;
        }
        case DataTypes.DropDownList: {
          const dataSource = this.column$.value?.dataSource || [...this._dropdownListDataSource];
          this._initOptions(dataSource);
          this.editForm.controls.defaultValue.setValue(dataSource[0]);
          break;
        }
        default: {
          this.editForm.controls.defaultValue.setValue(defaultText);
          break;
        }
      }
    });
   }

   public ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  public ngOnInit(): void {
    this.column$.pipe(takeUntil(this._destroy$)).subscribe(column => {
      if (column) {
        const { name, width, isPrimary, ...values } = column;
        if (values.dataSource?.length) {
          this._initOptions(values.dataSource);
        }
        values.minWidth = column.minWidth || defaultMinWidth
        values.dataSource = values.dataSource || [];
        this.isEdit = true;
        this.editForm.setValue(values);
      } else {
        this.isEdit = false;
        this.editForm.setValue({...this._initialValue});
      }
    });
  }

  public getControl(key: keyof IColumnInput): FormControl {
    return this.editForm.get(key) as FormControl;
  }

  public getDataSourceControl(i: number) {
    return this.dataSourceControl.controls[i] as FormControl;
  }

  public removeOption(i: number) {
    this.dataSourceControl.removeAt(i);
  }

  public addOption() {
    const newVal = `Option ${this.dataSourceControl.controls.length + 1}`;
    this.dataSourceControl.controls.push(this._fb.control(newVal, Validators.required));
    this.dataSourceControl.setValue(this.dataSourceControl.value.concat([newVal]));
  }

  private _initOptions = (values: string[]) => {
    this.editForm.controls.dataSource = this._fb.array(values.map(v => this._fb.control(v, Validators.required)));
  }

  // if there's a column param then editing else creating new column
  public openDialog(column?: IColumn): void {
    this.column$.next(column || null);
    this.ejDialog?.show();
  }

  public onOverlayClick = () => {
    this.ejDialog?.hide();
  }

  public columnNameValidating = (control: AbstractControl): ValidationErrors => {
    const existedCol = this._columnsService.getColumnByHeader(control.value as string);
    let errors: ValidationErrors = {};
    if (this.isEdit !== undefined && this._columnHeaderText && existedCol && !(control.value === this._columnHeaderText && this.isEdit)) {
      errors['repeat'] = { value: `The column with name '${control.value}' is already exists!` }
    }
    if (control.value === '') {
      errors['required'] = { value: 'This field is required' };
    }
    return errors;
  }

  public editColumn() {
    if (this.editForm.valid) {
      const newVal = this.editForm.value;
      if (newVal.dataType === DataTypes.DropDownList) {
        newVal.dataSource = this._optionsValues;
      }
      this._columnsService.saveColumn(newVal, this._field);
      this.ejDialog?.hide();
    }
  }

  public addColumn() {
    if (this.editForm.valid) {
      const newVal = this.editForm.value;
      if (newVal.dataType === DataTypes.DropDownList) {
        newVal.dataSource = this._optionsValues;
      }
      this._columnsService.saveColumn(newVal);
      this.ejDialog?.hide();
    }
  }
}
