import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  public get socketId() {
    return this._socket.ioSocket.id;
  }

  constructor(
    private _socket: Socket
  ) {
    this._socket.connect();
  }

  public getMessage$() {
    return this._socket.fromEvent<void>('need update').pipe(map((data) => data));
  }
}
