import { Injectable } from '@angular/core';
import { pairwise } from 'rxjs/operators';
import { DataManager } from '@syncfusion/ej2-data';
import { ColumnsService } from './columns.service';
import { ServerStorageService } from './server-storage.service';
import { IData, IDataInDataSource } from '../models/data';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private _data = new DataManager();

  public get data() {
    return this._data;
  }

  public loaded = false;

  public rowsInBuffer: IDataInDataSource[] = [];

  private _isCut: boolean = false;

  public dataServiceColumnSubscription: Subscription | null = null;

  constructor(
    private _serverStorageService: ServerStorageService,
    private _columnsService: ColumnsService,
  ) {
    this.dataServiceColumnSubscription = this._columnsService.columns$.pipe(pairwise()).subscribe(() => {
      this.loaded = false;
      this.getData();
    });
  }


  public getData():void {
    this._serverStorageService.getGridData().subscribe((savedData) => {
      this._data.dataSource.json = savedData;
      this.loaded = true;
    });
  }

  public filterChildren(data: IDataInDataSource[], filtered: IDataInDataSource[] = []): IDataInDataSource[] {
    if (data.length) {
      const maxLevel = data.reduce((acc, child) => {
        if (acc === -1) return child.level;
        return acc > child.level ? acc : child.level;
      }, -1);
      const leafs = data.filter(el => el.level === maxLevel);
      const possibleParents = data.filter(el => el.level === (maxLevel - 1));
      if (possibleParents.length) {
        const leafsToAdd = leafs.filter(leaf => {
          return !possibleParents.some(parent => {
            return parent.childRecords?.includes(leaf);
          });
        });
        return this.filterChildren(
          data.filter(el => !leafs.includes(el)),
          filtered.concat(leafsToAdd)
        );
      }
      return this.filterChildren(
        data.filter(el => el.level !== maxLevel),
        filtered.concat(leafs)
      );
    }
    return filtered;
  }

  public countRowIndex = (data: IDataInDataSource): number[] => {
    let res: number[] = [];
    let currEl = data;
    while (currEl?.parentItem) {
      const parentArr = currEl?.parentItem?.taskData?.subtasks;
      const index = parentArr?.indexOf(currEl.taskData);
      if (index !== undefined && index !== -1) {
        res.unshift(index);
      }
      currEl = currEl?.parentItem;
    }
    const rootArr = this.data.dataSource.json as IData[];
    const rootIndex = rootArr.indexOf(currEl.taskData);
    res.unshift(rootIndex);
    return res;
  }

  public editRow(index: number[], newValue: IData) {
    // it takes some time to close modal
    setTimeout(() => {
      this.loaded = false;
      this._serverStorageService.editRow(index, newValue).subscribe(() => {
        this.loaded = true;
      });
    }, 100);
  }

  public addAsChild(index: number[], newValue: IData) {
    this._serverStorageService.addAsChild(index, newValue).subscribe();
  }

  public addAsSubling(index: number[], newValue: IData, isNext: boolean) {
    this._serverStorageService.addAsSubling(index, newValue, isNext).subscribe();
  }

  public deleteRow(index: Array<number[]>) {
    this.loaded = false;
    this._serverStorageService.deleteRow(index).subscribe(() => {
      this.getData();
    });
  }

  public moveRows(fromArr: Array<number[]>, toArr: Array<number[]>) {
    this.loaded = false;
    this._serverStorageService.moveRows(fromArr, toArr).subscribe((newData) => {
      this._data.dataSource.json = newData;
      this.loaded = true;
    });
  }

  public copy(rows: IDataInDataSource[], withCut: boolean = false) {
    this.rowsInBuffer = rows;
    this._isCut = withCut;
  }

  public paste(to: number[], asChild: boolean) {
    if (this.rowsInBuffer) {
      const copiedIndicies = this.filterChildren([...this.rowsInBuffer]).map(this.countRowIndex);
      this.loaded = false;
      this._serverStorageService.pasteRows(copiedIndicies, to, this._isCut, asChild).subscribe((newData) => {
        this._data.dataSource.json = newData;
        this.loaded = true;
      });
    }
  }
}
