import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SocketService } from './socket.service';
import { IColumn } from '../models/column';
import { IData } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class ServerStorageService {

  private _serverBaseUrl = `${environment.serverAddress}:${environment.restPort}/`;

  private _columnsUrl = this._serverBaseUrl + 'columns';

  private _dataUrl = this._serverBaseUrl + 'data';

  constructor(
    private _httpClient: HttpClient,
    private _socketService: SocketService,
  ) {
  }

  public getGridData() {
    return this._httpClient.get<IData[]>(this._dataUrl);
  }

  public getColumnsData() {
    return this._httpClient.get<IColumn[]>(this._columnsUrl);
  }

  public editColumn(name: string, data: IColumn) {
    const socketId = this._socketService.socketId;
    return this._httpClient.post<IColumn[]>(this._columnsUrl + '/edit', { name, data, socketId });
  }

  public editRow(index: number[], newData: IData) {
    const { subtasks, ...data } = newData;
    const socketId = this._socketService.socketId;
    return this._httpClient.post<IData[]>(this._dataUrl + '/edit', { index, data, socketId });
  }

  public saveColumnData(data: IColumn[]) {
    const socketId = this._socketService.socketId;
    return this._httpClient.post(this._columnsUrl, { data, socketId });
  }

  public deleteRow(indexes: Array<number[]>) {
    const socketId = this._socketService.socketId;
    return this._httpClient.post(this._dataUrl + '/delete', { indexes, socketId });
  }

  public addAsChild(target: number[], value: IData) {
    const socketId = this._socketService.socketId;
    return this._httpClient.post(this._dataUrl + '/asChild', { target, value, socketId });
  }

  public addAsSubling(targetIndex: number[], data: IData, isNext: boolean) {
    const socketId = this._socketService.socketId;
    return this._httpClient.post(this._dataUrl + '/asSubling', { targetIndex, data, isNext, socketId });
  }

  public moveRows(fromArr: Array<number[]>, toArr: Array<number[]>): Observable<IData[]> {
    const socketId = this._socketService.socketId;
    return this._httpClient.post<IData[]>(this._dataUrl + '/move', { fromArr, toArr, socketId });
  }

  public pasteRows(fromArr: Array<number[]>, to: number[], isCut: boolean, asChild: boolean): Observable<IData[]> {
    const socketId = this._socketService.socketId;
    return this._httpClient.post<IData[]>(this._dataUrl + '/paste', { fromArr, to, isCut, asChild, socketId });
  }
}
