import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ServerStorageService } from './server-storage.service';
import { DataTypes, defaultStyles, IColumn, IColumnInput, Priority } from '../models/column';
import { defaultText } from '../constants/utils';
import { IData } from '../models/data';

export enum Fields {
  TaskID = 'taskID',
  TaskName = 'taskName',
  StartDate = 'startDate',
  EndDate = 'endDate',
  Duration = 'duration',
  Priority = 'priority'
}

@Injectable({
  providedIn: 'root'
})
export class ColumnsService {
  private _columns$ = new BehaviorSubject<IColumn[]>([]);
  public columns$ = this._columns$.asObservable();

  public loaded = false;

  private _defaultColumns: readonly IColumn[] = Object.freeze<IColumn[]>([{
    ...defaultStyles,
    name: Fields.TaskID,
    isPrimary: true,
    width: 120,
    headerText: 'Task ID',
    dataType: DataTypes.Num,
    defaultValue: 1,
  }, {
    ...defaultStyles,
    name: Fields.TaskName,
    isPrimary: false,
    minWidth: 190, // it looks like minWidth doesn't work
    width: 210,
    headerText: 'Task Name',
    dataType: DataTypes.Text,
    defaultValue: defaultText,
  }, {
    ...defaultStyles,
    name: Fields.StartDate,
    isPrimary: false,
    width: 170,
    headerText: 'Start Date',
    dataType: DataTypes.Date,
    format: 'yMd',
    defaultValue: new Date()
  }, {
    ...defaultStyles,
    name: Fields.EndDate,
    isPrimary: false,
    width: 170,
    headerText: 'End Date',
    dataType: DataTypes.Date,
    format: 'yMd',
    defaultValue: new Date()
  }, {
    ...defaultStyles,
    name: Fields.Duration,
    isPrimary: false,
    width: 140,
    dataType: DataTypes.Num,
    headerText: 'Duration',
    defaultValue: 0
  }, {
    ...defaultStyles,
    name: Fields.Priority,
    isPrimary: false,
    width: 120,
    dataType: DataTypes.DropDownList,
    dataSource: [Priority.High, Priority.Medium, Priority.Low],
    headerText: 'Priority',
    defaultValue: Priority.Low
  }]);

  public get defaultData(): IData {
    return this._columns$.value.reduce((accum, column) => {
      accum[column.name] = column.defaultValue;
      return accum;
    }, {} as Record<string, unknown>);
  }

  constructor(
    private _serverStorageService: ServerStorageService,
  ) {
    this.getColumns();
  }

  public getColumns() {
    this._serverStorageService.getColumnsData().subscribe(savedData => {
      this._columns$.next(savedData || [...this._defaultColumns]);
      this.loaded = true;
    });
  }

  public removeColumn(name: string) {
    const newVal = this._columns$.value.filter(c => c.name !== name);
    this.saveColumns(newVal);
  }

  public getColumn(field: string) {
    return this._columns$.value.find(c => field === c.name);
  }

  public getColumnByHeader(text: string) {
    return this._columns$.value.find(c => text === c.headerText);
  }

  private _editColumnField(field: string, fieldName: keyof IColumn,  newVal: unknown) {
    const columns = [...this._columns$.value];
    let columnInd = columns.findIndex(c => field === c.name);
    if (columnInd !== -1) {
      const newColumn = { ...columns[columnInd] };
      newColumn[fieldName] = newVal;
      columns[columnInd] = newColumn;
      this.saveColumns(columns);
    }
  }

  private _editColumn(field: string, newValue: IColumn) {
    this.loaded = false;
    this._serverStorageService.editColumn(field, newValue).subscribe((newColumns) => {
      this._columns$.next(newColumns);
      this.loaded = true;
    });
  }

  public editWidth(field: string,  newWidth: number) {
    this._editColumnField(field, 'width', newWidth);
  }

  public reorderColumn(field: string, newIndex: number) {
    const currCols = [...this._columns$.value];
    const currInd = currCols.findIndex(c => field === c.name);
    if (currInd !== -1) {
      const newCols = currCols.filter((_, i) => i !== currInd);
      newCols.splice(newIndex, 0, currCols[currInd]);
      this.saveColumns(newCols);
    }
  }

  private _generateColumnName(headerText: string) {
    return headerText.split(' ').reduce((acc, headerWord, index) => {
      const firstLatter = index ? headerWord[0].toUpperCase() : headerWord[0].toLowerCase();
      headerWord = firstLatter + headerWord.slice(1, headerWord.length);
      return acc + headerWord;
     }, '');
  }

  public saveColumn(newVal: IColumnInput, editedName?: string): void {
    const newColumn = {
      ...newVal,
      name: this._generateColumnName(newVal.headerText),
      width: newVal.minWidth,
      isPrimary: false,
    };
    if (editedName !== undefined) {
       this._editColumn(editedName, newColumn);
    } else {
      this.saveColumns(this._columns$.value.concat([newColumn]));
    }
  }

  public saveColumns(data: IColumn[]) {
    this._serverStorageService.saveColumnData(data).subscribe(() => {
      this._columns$.next(data);
    });
  }
}
