import { defaultBackgroundColor, defaultFontSize } from "../constants/utils";

export enum Priority {
  High = 'High',
  Medium = 'Normal',
  Low = 'Low',
}

export enum TextAlign {
  Right = 'Right', Left = 'Left', Center = 'Center'
}

export enum TextWrap {
  Normal = 'normal',
  BreakWord = 'break-word'
}

export enum DataTypes {
  Text = 'string',Num = 'numeric', Date = 'datepicker', Boolean = 'boolean', DropDownList = 'dropdown'
}

export enum InputTypes {
  Color = 'color'
}

export type IInputType = DataTypes | InputTypes;

export type IGeneralSettingsInput = {
  dataType: DataTypes;
  defaultValue: unknown;
  dataSource?: string[];
}

export type IGeneralSettings = IGeneralSettingsInput & {
  name: string;
}

export type IColumnStyles = {
  textAlign: TextAlign;
  fontSize: number;
  fontColor: string;
  backgroundColor: string;
  textWrap: TextWrap;
  minWidth?: number;
}

export const defaultStyles: IColumnStyles = Object.freeze({
  textAlign: TextAlign.Left,
  fontSize: defaultFontSize,
  fontColor: 'black',
  backgroundColor: defaultBackgroundColor,
  textWrap: TextWrap.Normal,
});

export type IColumnInput = IGeneralSettingsInput & IColumnStyles & {
  headerText: string;
}

export type IColumn = IGeneralSettings & IColumnStyles & {
  isPrimary: boolean;
  minWidth?: number;
  width?: number;
  headerText: string;
  dataSource?: string[]; // if dataType === DataTypes.DropDownList
  format?: string; // if dataType === DataTypes.Date
  [key: string]: unknown;
}

export const numPriority = [Priority.High, Priority.Medium, Priority.Low];
