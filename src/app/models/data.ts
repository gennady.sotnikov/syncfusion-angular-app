export type IData = {
  subtasks?: IData[];
  [key: string]: unknown;
}


export type IDataInDataSource = IData & {
  uniqueID: string;
  parentUniqueID: string;
  index: number;
  taskData: IData;
  level: number;
  childRecords?: IDataInDataSource[];
  hasChildRecords?: boolean;
  expanded?: boolean;
  parentItem?: IDataInDataSource;
}
