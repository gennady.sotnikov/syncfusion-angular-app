export const getDiffBetweenDatesInDays = (d1: Date, d2: Date) => {
  const diffTime = Math.abs(d2.valueOf() - d1.valueOf());
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  return diffDays;
}

export const generateRandomNumber = (from: number = 0, to: number = 100) => {
  return (Math.trunc(Math.random() * 1000000) % (to + 1)) + from;
};

export const rowHeight = 42;

export const extraGridHeight = 30;

export const copiedRowsColor = 'pink';

export const defaultFontSize = 12;

export const defaultBackgroundColor = 'rgba(0,0,0,0)';

export const defaultFontColor = 'black';

export const defaultText = 'Default Text';

export const defaultMinWidth = 130;
