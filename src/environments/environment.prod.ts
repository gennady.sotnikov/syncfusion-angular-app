export const environment = {
  production: true,
  serverAddress: 'http://65.21.61.26',
  restPort: 3000,
  socketPort: 3001
};
